<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak extends CI_Controller {
    
	
	public function zakatfitrah($id_zakat_fitrah) {
	
		$sql = "SELECT * FROM mjd_zakat_fitrah  WHERE id_zakat_fitrah='$id_zakat_fitrah'";
		$query = $this->db->query($sql);
		$data['field'] = $query->row();
		
		$this->load->view('cetak/v_zakat_fitrah_cetak_tt',$data);
	
     
    }
	
	
	public function zakatfitrah_lap() {
	
		$now=date("Y");
		
		$sql="SELECT * FROM mjd_zakat_fitrah  WHERE YEAR(date)='$now'";
		$query = $this->db->query($sql);
		$data['hasil'] = $query->result();
		
		$this->load->view('cetak/v_zakat_fitrah_cetak_lap',$data);
	
     
    }
	
	
	public function zakatfitrah_rekap() {
	
		$now=date("Y");
		
		$sql="select 
sum(jiwa) as jiwa_beras,
sum(total) as total_beras
from 
mjd_zakat_fitrah
where
category='Beras' and YEAR(date)='$now'";
		$query = $this->db->query($sql);
		$data['field'] = $query->row();
		
		
		$sql="select 
sum(jiwa) as jiwa_uang,
sum(total) as total_uang
from 
mjd_zakat_fitrah
where
category='Uang' and YEAR(date)='$now'";
		$query = $this->db->query($sql);
		$data['field2'] = $query->row();
		
		$this->load->view('cetak/v_zakat_fitrah_cetak_rekap',$data);
	
     
    }
	
	
	
	
	public function zakatmal($id_zakat_mal) {
	
		$sql = "SELECT * FROM mjd_zakat_mal  WHERE id_zakat_mal='$id_zakat_mal'";
		$query = $this->db->query($sql);
		$data['field'] = $query->row();
		
		$this->load->view('cetak/v_zakat_mal_cetak',$data);
	
     
    }
	
	
}