<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {
	function __construct(){
		parent::__construct();		
		$this->load->model('frontend/m_mosque_front');
	}

	public function index(){
		$data['mosque'] = $this->m_mosque_front->view_mosque_data()->result();
		$where = array(
			'category' => 'Infaq Mingguan'
		);
		$data['infaq_mingguan'] = $this->m_mosque_front->get_total_infaq($where)->row()->total;
		$where = array(
			'category' => "Infaq Jum'at"
		);
		$data['infaq_jumat'] = $this->m_mosque_front->get_total_infaq($where)->row()->total;
		$data['zakat_fitrah'] = $this->m_mosque_front->get_total_zakat()->row()->total;
		$data['zakat_mal'] = $this->m_mosque_front->get_total_zakat_mal()->row()->total;
		$this->load->view('frontend/v_report', $data);
		$this->load->view('frontend/inc/v_footer', $data);
	}
}