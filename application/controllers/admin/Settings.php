<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends AdminInterface {

	function __construct(){
		parent::__construct();		
		$this->load->model('install/m_mosque');
		$this->load->model('install/m_account');
	}

	public function index(){
		$data['mosque'] = $this->m_mosque->view_mosque_data()->result();
		$this->load->view('backend/v_settings', $data);
		$this->load->view('backend/inc/v_footer_maps.php', $data);
	}

	function exec_edit(){
		$config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= 10000;
		$this->load->library('upload', $config);
		
		$id = $this->input->post('id-mosque');
		$name = $this->input->post('mosque-name');
		$motto = $this->input->post('mosque-motto');
		$width = $this->input->post('mosque-width');
		$volume = $this->input->post('mosque-people');
		$about_short = $this->input->post('mosque-about-small');
		$about_long = $this->input->post('mosque-about-long');
		$greeting = $this->input->post('mosque-greeting');

		$this->upload->do_upload('mosque-slider');
		$mosque_slider = $this->upload->data()['file_name'];

		$this->upload->do_upload('mosque-greeting-pic');
		$mosque_greeting_pic = $this->upload->data()['file_name'];

		$this->upload->do_upload('mosque-logo');
		$mosque_logo = $this->upload->data()['file_name'];

		$data = array(
			'name' => $name,
			'motto' => $motto,
			'width' => $width,
			'volume' => $volume,
			'about_short' => $about_short,
			'about_long' => $about_long,
			'greeting' => $greeting,
			'slider_pic' => $mosque_slider,
			'greeting_pic' => $mosque_greeting_pic,
			'logo' => $mosque_logo
		);
	 
		$where = array(
			'id_mosque' => $id
		);
	 
		$this->m_mosque->update_mosque_data($where, $data);
		redirect('admin/settings/');
	}

	function exec_edit2() {
		$id = $this->input->post('id-mosque');
		$email = $this->input->post('mosque-email');
		$phone = $this->input->post('mosque-phone');
		$address = $this->input->post('mosque-address');
		$city = $this->input->post('mosque-city');
		$province = $this->input->post('mosque-province');
		$country = $this->input->post('mosque-country');
		$postcode = $this->input->post('mosque-postcode');
		$lat = $this->input->post('mosque-latitude');
		$lon = $this->input->post('mosque-longitude');

		$data = array(
			'email' => $email,
			'phone' => $phone,
			'address' => $address,
			'city' => $city,
			'province' => $province,
			'country' => $country,
			'postcode' => $postcode,
			'lat' => $lat,
			'lon' => $lon
		);	 
		$where = array(
			'id_mosque' => $id
		);	 
		$this->m_mosque->update_mosque_data($where, $data);
		redirect('admin/settings/');
	}

	public function gallery(){
		$data['gallery'] = $this->m_mosque->view_gallery_data()->result();
		$this->load->view('backend/v_gallery', $data);
	}

	public function exec_gallery() {
		$config['upload_path'] = './assets/uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= 10000;
		$this->load->library('upload', $config);

		$this->upload->do_upload('image');
		$image_name = $this->upload->data()['file_name'];
		$image_desc = $this->input->post('image-desc');

		$data = array(
			'file_name' => $image_name,
			'caption' => $image_desc
		);
		$this->m_mosque->insert_gallery_data($data);
		redirect('admin/settings/gallery');
	}
	
	public function exec_delete_gallery($id_gallery) {
		$where = array(
			'id_gallery' => $id_gallery
		);
		$this->m_mosque->delete_gallery_data($where);
		redirect('admin/settings/gallery');
	}
}