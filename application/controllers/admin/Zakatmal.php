<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zakatmal extends AdminInterface {
	function __construct(){
		parent::__construct();		
		$this->load->model('backend/m_zakat_mal');
	}

	public function index(){
		$data['zakat'] = $this->m_zakat_mal->view_zakat_mal_data()->result();
		$this->load->view('backend/v_zakat_mal_view', $data);
	}

	public function add(){
		$this->load->view('backend/v_zakat_mal_add');
	}

	function exec_add(){
		$date = $this->input->post('date-zakat');
		$name = $this->input->post('name-zakat');
		$total = $this->input->post('number-zakat');
		$info = $this->input->post('text-zakat');

		$data = array(
			'date' => $date,
			'name' => $name,
			'total' => $total,
			'info' => $info
		);
		$this->m_zakat_mal->insert_zakat_mal_data($data);
		redirect('admin/zakatmal/');
	}

	function delete($id){
		$where = array('id_zakat_mal' => $id);
		$this->m_zakat_mal->delete_zakat_mal_data($where);
		redirect('admin/zakatmal/');
	}
}