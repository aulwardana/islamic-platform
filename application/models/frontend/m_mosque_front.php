<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class M_mosque_front extends CI_Model{
	function view_mosque_data(){
		return $this->db->get('mjd_mosque');
	}

	function count_user_data(){
		$query = $this->db->get('mjd_users');
		return $query->num_rows();
	}

	function count_event_data(){
		$query = $this->db->get('mjd_task');
		return $query->num_rows();
	}

	function count_team_data(){
		$query = $this->db->get('mjd_takmir');
		return $query->num_rows();
	}

	function get_team_name($where){
		$this->db->select('name');
		$this->db->limit(1);
		return $this->db->get_where('mjd_takmir',$where);
	}

	function get_team_data(){
		return $this->db->get('mjd_takmir');
	}

	function get_total_infaq($where) {
		$this->db->select_sum('total');
		$this->db->where($where);
		return $this->db->get('mjd_infaq');
	}

	function get_total_zakat() {
        $this->db->select_sum('total');
        return $this->db->get('mjd_zakat_fitrah');
	}
	
	function get_total_zakat_mal() {
        $this->db->select_sum('total');
        return $this->db->get('mjd_zakat_mal');
    }
}