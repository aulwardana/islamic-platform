<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
    $this->load->view('backend/inc/v_sidebar.php');
?>

        <div class="page-wrapper">
        <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Masukkan Data Mustahik</h3> </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Mustahik</a></li>
                        <li class="breadcrumb-item active">Masukkan Data Mustahik</li>
                    </ol>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                            <div class="card-body">
                                <form action="<?php echo base_url(). 'admin/mustahik/exec_add/'; ?>" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label">Tanggal</label>
                                        <div class="col-sm-8">
                                            <input type="date" name="date-mustahik" class="form-control" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label">Nama</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="name-mustahik" class="form-control" placeholder="Pak Khusnul">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label">Jenis</label>
                                        <div class="col-sm-8">
                                            <select name="category-mustahik" class="form-control">
                                                <option>Beras</option>
                                                <option>Uang</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label">Jumlah</label>
                                        <div class="col-sm-8">
                                            <input type="number" name="number-mustahik" class="form-control input-default " placeholder="2.5">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-8 control-label">Keterangan</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="text-mustahik" class="form-control input-default " placeholder="-">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Masukkan Data Zakat Mal</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php
    $this->load->view('backend/inc/v_footer.php');
?>