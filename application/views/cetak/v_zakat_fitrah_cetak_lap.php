<!--<script>


window.print();

</script>-->
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=LapZakatFitrah.xls");
?>

<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>
<h3 align="center">Laporan Penerimaan Zakat Fitrah </h3>
<table width="100%" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td rowspan="2"><strong>No</strong></td>
    <td rowspan="2"><strong>Tanggal</strong></td>
    <td rowspan="2"><strong>Nama</strong></td>
    <td rowspan="2"><strong>Jiwa</strong></td>
    <td colspan="2"><strong>Zakat Fitrah</strong></td>
    <td rowspan="2"><strong>Keterangan</strong></td>
  </tr>
  <tr>
    <td><strong>Beras (Kg)</strong></td>
    <td><strong>Uang (Rp)</strong></td>
  </tr>
  <?php 
  $i=1;
  $tb=0;
  $tu=0;
  foreach ($hasil as $row)
{

 if ($row->category=="Beras") { $tb+=$row->total;  }
   if ($row->category=="Uang") { $tu+= $row->total;  }
  ?>
  <tr>
    <td>&nbsp;<?php echo $i; ?></td>
    <td><?php  echo $row->date; ?></td>
    <td><?php  echo $row->name; ?></td>
    <td><?php  echo $row->jiwa; ?></td>
    <td><?php  if ($row->category=="Beras") { $t1=$row->total; echo $t1; } ?></td>
    <td><?php  if ($row->category=="Uang") { $t2= $row->total; echo $t2; } ?></td>
    <td><?php  echo $row->info; ?></td>
  </tr>
  <?php $i++; } ?>
  <tr>
    <td colspan="4"><strong>Jumlah</strong></td>
    <td><?php   echo $tb;  ?></td>
    <td><?php   echo $tu;  ?></td>
    <td>&nbsp;</td>
  </tr>
</table>
<br />
