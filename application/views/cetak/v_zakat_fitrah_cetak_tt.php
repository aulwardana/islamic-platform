<script>


window.print();

</script><style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif;
}
-->
</style>

<table width="100%"  style="border:solid 1px #000000;">
  <tr>
    <td colspan="2"><h3 align="center">TANDA TERIMA ZAKAT FITRAH</h3></td>
  </tr>
  <tr>
    <td colspan="2"><hr /></td>
  </tr>
  <tr>
    <td width="20%">Sudah diterima dari</td>
    <td width="80%">:</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>Tanggal</td>
    <td>: <?php echo isset($field->date)?$field->date:''; ?></td>
  </tr>
  <tr>
    <td>Nama</td>
    <td>: <?php echo isset($field->name)?$field->name:''; ?></td>
  </tr> 
  <tr>
    <td>Jenis </td>
    <td>: <?php echo isset($field->category)?$field->category:''; ?></td>
  </tr>
  <tr>
    <td>Jumlah Jiwa</td>
    <td>: <?php echo isset($field->jiwa)?$field->jiwa:''; ?></td>
  </tr>
  <tr>
    <td>Jumlah</td>
    <td>: <?php echo isset($field->total)?$field->total:''; ?></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>: <?php echo isset($field->info)?$field->info:''; ?></td>
  </tr>
</table>
<p align="right">Penerima ,</p>
<p align="right">&nbsp;</p>
<p align="right">-----------------------------------------------<br />
Nama dan Tandatangan</p>
